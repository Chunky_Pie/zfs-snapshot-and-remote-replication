
# ZFS Snapshot And Replication



**<span class="underline">Over-View</span>**

This script is broken up into three parts.  A configuration file, a driver script and a library script.

-   Configuration File - zfs_backups.conf
-   Driver Script - zfs_backups
-   Library Script - libzfsmain

When configured and executed correctly, this script will take a snapshot of your datasets and replicate them to a remote host using `zfs send/receive`, piped through `mbuffer`.  SSH is used only to confirm what exists on the remote host (pools, dependencies, etc, etc) and to start the `zfs receive` command.

As such if you want to use this script to replicate to hosts that are *NOT* on your LAN, it's suggested you use [Nebula](https://github.com/slackhq/nebula) or [Tailscale](https://tailscale.com/) to implement a mesh VPN.

<span class="underline">Dependencies</span>

-   bash version 4 or higher (source host only)
-   netcat (nc) (both source and destination host)
-   mbuffer (both source and destination host)
-   ZFS (both source and destination host)

**<span class="underline">Configuration File</span>**

The configuration file is where the user defines what datasets get snapshot, where those snapshots get replicated to (currently only supports to a remote host - not a local zpool) and how many snapshots to keep for each dataset (currently only supports a setting that applies to all datasets)

Although the config file is created to be "user friendly", it loosely follows the format of bash string, array and associative array variable declarations.  There also exists an "EXAMPLES" section at the bottom of the config file to give the user samples of what the filled out sections should look like.  I will review some of those configs below:

<span class="underline">DATASETS</span>

This is where the user defines which datasets (using a pool/dataset format) they want snapshot and replicated.  This is contained in a bash array variable but the user is expected to define their datasets with-in the blank space and each on a seperate line.  i.e. -

    _DATASETS=(
    
    tank1/media
    tank2/homeassistant
    tank2/pihole
    tank2/plex
    tank2/nextcloud
    
    )

<span class="underline">KEEP</span>

Here is where the user defines how many snapshots to keep for each dataset.  This is contained in a bash string variable, evaluated as an integer (arithmetic expression).  Like all the values in this config file, the user is expected to define their values with-in the blank space and each on a seperate line (if more than one value can be defined.)

    let _KEEP='
    
    7
    
    '

<span class="underline">DESTINATION_POOLS</span>

This is where the user defines what pools on the remote host they want their datasets replicated to.  It uses a hash map to define what datasets from pool A [key] go to which pool on the remote host [value].  This is contained in a bash associative array and like all values, should be defined with-in the blank space and each on a seperate line.  In the below example, keys (the source pool) are defined with-in [square brackets] and the values (remote pool) are with in "double quotes":

    declare -Ag _DESTINATION_POOLS=(
    
    [tank1]="backup_tank1"
    [tank2]="backup_tank2"
    
    )

The rest of the settings that can be configured, weren't explained here but can be filled out following the same format and will either bash array, string or associative arrray variables.

**<span class="underline">Driver Script</span>**

`zfs_backups` is the script invoked to start the backups.  It's mainly designed to process any flags (explained later) and execute the "main" function which runs all the tasks associated with backing up the configured datasets.

This is also where all the functions housed in the library script are sourced in.  The script contains a "help" flag that you can use to display all the available flags in the script:

    ./zfs_backups -h
    
       usage: ./zfs_backups [options]
    
            OPTIONS
    
                -h	  Print usage info
    
                -d	  Turn on debugging
    
                -t    Tests ZFS Backup Configuration only
    
                -T	  Same as -t plus runs through all other tests and does a dry run of all snapshots to be trimmed, only

**<span class="underline">Library Script</span>**

Lastly is `libzfsmain`.  Here is where all the functions used to implement the zfs backups, are housed.  The intent behind keeping these functions isolated to their own file, was to help keep things neat and organized in a way that allowed the user to read how all the logic is executed.  So far (this may be off by a step or two) these functions are listed in the order they are executed.  Each step in the backup process is collapsed into it's own tight function (as best as I could, some functions are bigger than I'd like).  Also, some functions themselves are just "driver" functions that call other functions to complete the larger operation of said "function".

`libzfsmain` has thorough comments at the top of the script describing what each function does in addition to necessary comments throught the functions themselves.

**<span class="underline">Logging</span>**

This script will output a `zfs_backups.log` file in the same directory this script is ran from and will continue to append to this file everytime the script is ran.  It will show Messages for every successful action the script takes with dates and time stamps in addition to Errors if those actions or a "check" fails (also date and time stamped).  Log file example below:

    ##################################################--BEGIN LOG--#################################################
    
    [ MESSAGE 04/11/24 00:00 ] =============== The following datasets will be backuped! ===============
    
            test_pool/dataset_test
    
    
    [ MESSAGE 04/11/24 00:00 ] =============== All send permissions on source pools are valid! ===============
    
    
    [ MESSAGE 04/11/24 00:00 ] =============== SSH Test To remote_host Successful! ===============
    
    
    [ MESSAGE 04/11/24 00:00 ] =============== All remote pools are valid! ===============
    
    
    [ MESSAGE 04/11/24 00:00 ] =============== All receive permissions on remote pools are valid! ===============
    
    
    [ MESSAGE 04/11/24 00:00 ] =============== All utilities required for ZFS replication exist on local and remote host! ===============
    
    
    [ MESSAGE 04/11/24 00:00 ] =============== Port 9090 is open and ready for ZFS replication! ===============
    
    
    [ MESSAGE 04/11/24 00:00 ] =============== Initiated ZFS Receive on remote_host succuessfully! ===============
    
    
    [ MESSAGE 04/11/24 00:00 ] =============== Created snapshot of test_pool/dataset_test@auto_2024.04.11 successfully! ===============
    
    
    [ MESSAGE 04/11/24 00:09 ] =============== ZFS Send of test_pool/dataset_test@auto_2024.04.11 snapshot finished successfully! ===============

**<span class="underline">Suggested How To Use</span>**

<span class="underline">ZFS and SSH User</span>  
The "_SSH_USER" value you configure in the conf file is not only the user who can ssh into the remote host but also the user who needs to have the appropriate permission set on the remote zfs host to receive snapshot replications.  The user who executes the script must have the appropriate permissions set on the local zfs host to take snapshots and send them.  All required permissions can be learned and confirmed (nice, that rhymed) by running the script with the appropriate test flags.

<span class="underline">Testing The Config</span>  
Once you've filled out the `zfs_backups.conf` file appropriately, you can run the `zfs_backups` script with the `-t` option to test it for any errors in the datasets you configured.  It will output to the screen all the datasets you chose for backups and if no errors are printed, then all your datasets are confirmed to exist (helps to catch typos).

    ./zfs_backups -t
    
    [ MESSAGE 04/13/24 20:57 ] =============== The following datasets will be backuped! ===============
    
            tank1/media
            tank2/homeassistant
            tank2/pihole
            tank2/plex
            tank2/nextcloud

You can also run `zfs_backups` with the option `-T` which tests the rest of the values in your configuration but also runs additional checks to ensure a backup will happen successfully.  Below are a list of some of those tests that are conducted:

-   Are your send/receive permissions set appropriately on both source and destination pools?
-   Can ssh connections be made with the provided ssh key, ssh user and remote host?
-   Do the remote pools configured actually exist on the remote host?
-   Are all dependencies met?  (i.e. nc and mbuffer utilities?)
-   A dry run of what snapshots will be trimmed will also be output to the screen (if any exist)

etc, etc, etc.  Again the results of these tests are output to the screen, along with any errors:

    ./zfs_backups -T
    
    [ MESSAGE 04/13/24 20:57 ] =============== The following datasets will be backuped! ===============
    
            tank1/media
            tank2/homeassistant
            tank2/pihole
            tank2/plex
            tank2/nextcloud
    
    [ MESSAGE 04/13/24 20:57 ] =============== All send permissions on source pools are valid! ===============
    
    
    [ MESSAGE 04/13/24 20:57 ] =============== SSH Test To remote_host Successful! ===============
    
    
    [ MESSAGE 04/13/24 20:57 ] =============== All remote pools are valid! ===============
    
    
    [ MESSAGE 04/13/24 20:57 ] =============== All receive permissions on remote pools are valid! ===============
    
    
    [ MESSAGE 04/13/24 20:57 ] =============== All utilities required for ZFS replication exist on local and remote host! ===============
    
    
    [ MESSAGE 04/13/24 20:57 ] =============== Port 9090 is open and ready for ZFS replication! ===============

<span class="underline">Automation</span>

Automation is acheived by running this script as a cronjob and how *OFTEN* datasets are snapshot and replicated are determined by the frequency of that cronjob.  This script was designed to automate the backup process through a configuration file and a script that when ran, will take care of the entire snapshot and replication process for each dataset, to a remote host.

This script can also be automated via a SystemD timer and service (not included.)

<span class="underline">Opportunities For Improvement</span>

A lot of this scripts design was inspired by [Jim Salter's](https://mercenarysysadmin.com/) [Sanoid Project](https://github.com/jimsalterjrs/sanoid) and it is *FAR* from as robust.  This script does not allow for defining fine grained snapshot policies, like keeping *n* daily, weekly, monthly or yearly snapshots for each dataset nor does it allow you to configure replication to a different pool on the same host.  It's more like a *shotgun* solution for routinely backing up all or some of your datasets to a remote host.

My motivation for writting this script was in two parts due to needing to implement an automated way of backing up my data and spend time on a project I could do as time allowed while, taking a much needed break my studies.  I hope it is useful to you and welcome any (although unlikely) contributions/suggestions.

